﻿namespace MyExplorer
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.PauseBtn = new System.Windows.Forms.Button();
            this.StopBtn = new System.Windows.Forms.Button();
            this.timePassed = new System.Windows.Forms.Label();
            this.filesPassed = new System.Windows.Forms.Label();
            this.curFile = new System.Windows.Forms.Label();
            this.timePassedMod = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.filesPassedMod = new System.Windows.Forms.Label();
            this.curFileMod = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 12);
            this.progressBar1.Maximum = 0;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(295, 23);
            this.progressBar1.Step = 1;
            this.progressBar1.TabIndex = 0;
            // 
            // PauseBtn
            // 
            this.PauseBtn.Location = new System.Drawing.Point(232, 53);
            this.PauseBtn.Name = "PauseBtn";
            this.PauseBtn.Size = new System.Drawing.Size(75, 23);
            this.PauseBtn.TabIndex = 1;
            this.PauseBtn.Text = "Pause";
            this.PauseBtn.UseVisualStyleBackColor = true;
            this.PauseBtn.Click += new System.EventHandler(this.PauseBtn_Click);
            // 
            // StopBtn
            // 
            this.StopBtn.Location = new System.Drawing.Point(232, 82);
            this.StopBtn.Name = "StopBtn";
            this.StopBtn.Size = new System.Drawing.Size(75, 23);
            this.StopBtn.TabIndex = 2;
            this.StopBtn.Text = "Stop";
            this.StopBtn.UseVisualStyleBackColor = true;
            this.StopBtn.Click += new System.EventHandler(this.StopBtn_Click);
            // 
            // timePassed
            // 
            this.timePassed.AutoSize = true;
            this.timePassed.Location = new System.Drawing.Point(9, 38);
            this.timePassed.Name = "timePassed";
            this.timePassed.Size = new System.Drawing.Size(70, 13);
            this.timePassed.TabIndex = 3;
            this.timePassed.Text = "Time passed:";
            // 
            // filesPassed
            // 
            this.filesPassed.AutoSize = true;
            this.filesPassed.Location = new System.Drawing.Point(9, 58);
            this.filesPassed.Name = "filesPassed";
            this.filesPassed.Size = new System.Drawing.Size(68, 13);
            this.filesPassed.TabIndex = 4;
            this.filesPassed.Text = "Files passed:";
            // 
            // curFile
            // 
            this.curFile.AutoSize = true;
            this.curFile.Location = new System.Drawing.Point(9, 75);
            this.curFile.Name = "curFile";
            this.curFile.Size = new System.Drawing.Size(60, 13);
            this.curFile.TabIndex = 5;
            this.curFile.Text = "Current file:";
            // 
            // timePassedMod
            // 
            this.timePassedMod.AutoSize = true;
            this.timePassedMod.Location = new System.Drawing.Point(86, 38);
            this.timePassedMod.Name = "timePassedMod";
            this.timePassedMod.Size = new System.Drawing.Size(33, 13);
            this.timePassedMod.TabIndex = 6;
            this.timePassedMod.Text = "0 sec";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // filesPassedMod
            // 
            this.filesPassedMod.AutoSize = true;
            this.filesPassedMod.Location = new System.Drawing.Point(84, 58);
            this.filesPassedMod.Name = "filesPassedMod";
            this.filesPassedMod.Size = new System.Drawing.Size(13, 13);
            this.filesPassedMod.TabIndex = 7;
            this.filesPassedMod.Text = "0";
            // 
            // curFileMod
            // 
            this.curFileMod.AutoSize = true;
            this.curFileMod.Location = new System.Drawing.Point(76, 75);
            this.curFileMod.MaximumSize = new System.Drawing.Size(160, 50);
            this.curFileMod.Name = "curFileMod";
            this.curFileMod.Size = new System.Drawing.Size(10, 13);
            this.curFileMod.TabIndex = 8;
            this.curFileMod.Text = "-";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 118);
            this.Controls.Add(this.curFileMod);
            this.Controls.Add(this.filesPassedMod);
            this.Controls.Add(this.timePassedMod);
            this.Controls.Add(this.curFile);
            this.Controls.Add(this.filesPassed);
            this.Controls.Add(this.timePassed);
            this.Controls.Add(this.StopBtn);
            this.Controls.Add(this.PauseBtn);
            this.Controls.Add(this.progressBar1);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form2";
            this.TopMost = true;
            this.Shown += new System.EventHandler(this.Form2_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button PauseBtn;
        private System.Windows.Forms.Button StopBtn;
        private System.Windows.Forms.Label timePassed;
        private System.Windows.Forms.Label filesPassed;
        private System.Windows.Forms.Label curFile;
        private System.Windows.Forms.Label timePassedMod;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label filesPassedMod;
        private System.Windows.Forms.Label curFileMod;
    }
}