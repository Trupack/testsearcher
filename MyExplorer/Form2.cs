﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace MyExplorer
{
    public partial class Form2 : Form
    {
        Form1 parent;
        bool isStoped = false;
        Stopwatch stopwatch = new Stopwatch();
        delegate void InvokeDelegate();
        Thread searchThread;
        bool isPause;

        public Form2()
        {
            InitializeComponent();
        }

        public Form2(Form1 parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        private void Form2_Shown(object sender, EventArgs e)
        {
            timer1.Start();
            stopwatch.Start();
            searchThread = new Thread(searchMetod);
            searchThread.Start();
        }

        private TreeNode CreateDirectoryNode(DirectoryInfo directoryInfo)
        {
            var directoryNode = new TreeNode(directoryInfo.Name);
            try
            {
                foreach (var directory in directoryInfo.GetDirectories())
                {
                    if (isStoped)
                        return directoryNode;
                    directoryNode.Nodes.Add(CreateDirectoryNode(directory));
                }

                FileInfo[] tmp;
                if (parent.txtFile.Text != "")
                {
                    tmp = directoryInfo.GetFiles(parent.txtFile.Text);
                }
                else
                    tmp = directoryInfo.GetFiles();

                foreach (var file in tmp)
                {
                    if (isStoped)
                        return directoryNode;

                    if (IsHandleCreated)
                        Invoke(new InvokeDelegate(() => curFileMod.Text = file.Name));
                    else
                        curFileMod.Text = file.Name;
                    if (parent.searchedText.Text != "")
                    {
                        string tmpText = File.ReadAllText(file.FullName);
                        if (tmpText.Contains(parent.searchedText.Text))
                        {
                            directoryNode.Nodes.Add(new TreeNode(file.Name));
                        }
                        Invoke(new InvokeDelegate(() => progressBar1.Value++));
                    }
                    else
                    {
                        directoryNode.Nodes.Add(new TreeNode(file.Name));
                        Invoke(new InvokeDelegate(() => progressBar1.Value++));
                    }

                }
            }
            catch (OutOfMemoryException e) { }
            return directoryNode;
        }

        void SetMaxProgrBar(DirectoryInfo directoryInfo)
        {
            var directoryNode = new TreeNode(directoryInfo.Name);
            foreach (var directory in directoryInfo.GetDirectories())
            {
                if (isStoped)
                    return;
                SetMaxProgrBar(directory);
            }

            if (parent.txtFile.Text != "")
                Invoke(new InvokeDelegate(() => progressBar1.Maximum += directoryInfo.GetFiles(parent.txtFile.Text).Length));
            else
                Invoke(new InvokeDelegate(() => progressBar1.Maximum += directoryInfo.GetFiles().Length));

        }


        private void StopBtn_Click(object sender, EventArgs e)
        {
            isStoped = true;

            if (isPause)
                UnpauseSearch();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!isPause)
            {
                timePassedMod.Text = stopwatch.Elapsed.TotalSeconds.ToString("#.##") + " sec";
                filesPassedMod.Text = progressBar1.Value.ToString();
            }
        }

        private void SearchEnd()
        {
            if (IsHandleCreated)
            {
                Invoke(new InvokeDelegate(() => StopBtn.Enabled = false));
                Invoke(new InvokeDelegate(() => PauseBtn.Enabled = false));
                Invoke(new InvokeDelegate(() => progressBar1.Value = progressBar1.Maximum));
                Invoke(new InvokeDelegate(() => timePassedMod.Text = stopwatch.Elapsed.TotalSeconds.ToString("#.##") + " sec"));
                Invoke(new InvokeDelegate(() => filesPassedMod.Text = progressBar1.Value.ToString()));

            }
            else
            {
                StopBtn.Enabled = false;
                PauseBtn.Enabled = false;
                progressBar1.Value = progressBar1.Maximum;
                timePassedMod.Text = stopwatch.Elapsed.TotalSeconds.ToString("#.##") + " sec";
                filesPassedMod.Text = progressBar1.Value.ToString();
            }
            isStoped = true;
            timer1.Stop();
            stopwatch.Stop();
        }

        private void PauseBtn_Click(object sender, EventArgs e)
        {
            if (!isPause)
            {
                searchThread.Suspend();
                PauseBtn.Text = "Unpause";
                stopwatch.Stop();
                isPause = true;
            }
            else
            {
                UnpauseSearch();
            }
        }

        private void UnpauseSearch()
        {
            searchThread.Resume();
            PauseBtn.Text = "Pause";
            stopwatch.Start();
            isPause = false;
        }

        private void FindRecursive(TreeNode treeNode, List<TreeNode> list)
        {
            if (treeNode.Nodes.Count == 0 && !Regex.Match(treeNode.Text, @"\.[0-9a-zA-z]+$").Success)
            {
                list.Add(treeNode);
            }
            foreach (TreeNode tn in treeNode.Nodes)
            {
                FindRecursive(tn, list);
            }
        }



        private void searchMetod()
        {
            DirectoryInfo dirInfo = new DirectoryInfo(parent.sSelectedPath);
            SetMaxProgrBar(dirInfo);
            TreeNode tmpNode = CreateDirectoryNode(dirInfo);

            while (true)
            {
                var list = new List<TreeNode>();
                FindRecursive(tmpNode, list);
                if (list.Count == 0)
                    break;
                foreach (TreeNode tn in list)
                    tmpNode.Nodes.Remove(tn);
            }

            Invoke(new InvokeDelegate(() => parent.treeView1.Nodes.Clear()));
            Invoke(new InvokeDelegate(() => parent.treeView1.Nodes.Add(tmpNode)));

            SearchEnd();
        }
    }
}