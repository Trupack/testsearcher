﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyExplorer
{
    public partial class Form1 : Form
    {
        public string sSelectedPath;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            txtFile.Enabled = false;
            btnSearch.Text = "Searching...";
            Form2 newForm = new Form2(this);
            newForm.ShowDialog(this);

            btnSearch.Text = "Search";
            this.Cursor = Cursors.Default;
            txtFile.Enabled = true;
        }

        private void Form1_Load(object sender, System.EventArgs e)
        {
            sSelectedPath = Properties.Settings.Default.Location;
            textPath.Text = sSelectedPath;
            txtFile.Text = Properties.Settings.Default.FileName;
            searchedText.Text = Properties.Settings.Default.TextInFile;
        }

        private void btnFileDialog_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK)
            {
                sSelectedPath = folderBrowserDialog1.SelectedPath;
                textPath.Text = sSelectedPath;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Location = sSelectedPath;
            Properties.Settings.Default.FileName = txtFile.Text;
            Properties.Settings.Default.TextInFile = searchedText.Text;
            Properties.Settings.Default.Save();
        }
    }
}
